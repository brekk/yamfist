import execa from "execa"

import { trace } from "xtrace"
import { matches, testCLI, resolveFrom } from "sinew/testing"

const resolve = resolveFrom(__dirname)

const EXECUTABLE = resolve("..", "yamfist.js")
const OUTFILE = resolve("..", "package.yml")

const RUN = {
  pkg: {
    j2y: {
      stdout: [EXECUTABLE, `--file`, `--to`, `yaml`, `package.json`],
      file: [
        EXECUTABLE,
        `--file`,
        `--to`,
        `yaml`,
        `package.json`,
        `-o`,
        OUTFILE
      ]
    },
    y2j: {
      stdout: [EXECUTABLE, "--file", "--to", "json", "package.yml"]
    }
  }
}

beforeAll(() => {
  const [exe, ...args] = RUN.pkg.j2y.file
  return execa(exe, args)
    .catch(trace("error!"))
    .then(() => console.log("package.yml written"))
})

testCLI(RUN.pkg.j2y.stdout, RUN.pkg.j2y.stdout.join(" "), matches)
testCLI(RUN.pkg.y2j.stdout, RUN.pkg.y2j.stdout.join(" "), matches)
