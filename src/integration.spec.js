import path from "path"

import { is, matches, testCommand, testShell } from "sinew/testing"
const here = x => path.resolve(__dirname, "..", x)
const yamfist = here("yamfist.js")
const pkg = here("package.json")
const pkgYaml = here("package.yml")

const RUN = {
  one: `echo '{"cool": {"very cool": {"so cool": false}}}' | ${yamfist} --to yaml`,
  two: [yamfist, `--to`, `yaml`, `--file`, pkg],
  three: [yamfist, `--to`, `json`, `--file`, pkgYaml]
}

testShell(
  RUN.one,
  "stdin | yamfist",
  is(`cool:
  very cool:
    so cool: false
`)
)
testCommand(RUN.two, matches)
testCommand(RUN.three, matches)
