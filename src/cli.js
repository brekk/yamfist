import { map, curry, pipe, chain } from "ramda"
import { processAsync } from "sinew/cli"

// import { trace } from "xtrace"

import { parseJSON, parseYAML, toYAML } from "./convert"

/**
 * Convert json to yaml
 * @public
 * @method json2yaml
 * @param {object} opts - configuration object
 * @param {string} source - instructions to parse
 * @return {Future<string>} future-wrapped string
 */
export const json2yaml = processAsync(
  curry((opts, source) =>
    pipe(
      chain(parseJSON),
      map(toYAML(opts))
    )(source)
  )
)

/**
 * Convert yaml to json
 * @public
 * @method yaml2json
 * @param {object} opts - configuration object
 * @param {string} source - instructions to parse
 * @return {Future<string>} future-wrapped json
 */
export const yaml2json = processAsync(
  curry((opts, source) => map(parseYAML(opts))(source))
)
