import { json2yaml, yaml2json } from "./cli"

test("json2yaml", done => {
  return json2yaml(
    {},
    '{"cool": "so cool", "yeah": { "it": { "is": {"json": true}}}}'
  ).fork(done, v => {
    expect(v).toEqual(`cool: so cool
yeah:
  it:
    is:
      json: true
`)
    done()
  })
})
test("yaml2json", done => {
  const input = `cool: so cool
yeah:
  it:
    is:
      json: true`
  return yaml2json({}, input).fork(done, x => {
    expect(x).toEqual({ cool: "so cool", yeah: { it: { is: { json: true } } } })
    done()
  })
})
// test(`json2yaml - package.yml`, done => {
//   return yaml2json(
//     { file: true, output: path.resolve(__dirname, "..", "package.yml") },
//     path.resolve(__dirname, "..", "package.json")
//   ).fork(done, x => {
//     expect(x).toBeFalsy()
//     done()
//   })
// })
