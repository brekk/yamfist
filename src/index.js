import argify from "yargs-parser"
import { prop, curry, pipe } from "ramda"
import supportsColor from "supports-color"

// import sinew from "sinew"
import { helpWithOptions } from "sinew/help"
import { makeInspector, makeTracer } from "sinew/log"

import { json2yaml, yaml2json } from "./cli"
import { CONFIG } from "./constants"

// const { helpWithOptions } = sinew.help
// const { makeInspector, makeTracer } = sinew.log

const THIS = "yamfist:core"
const trace = makeTracer(THIS)
const inspect = makeInspector(THIS)

const args = curry((opts, x) => argify(x, opts))

const die = data => e => {
  console.warn(data)
  console.warn(e)
  process.exit(1)
}

const help = helpWithOptions(CONFIG)

const shutOffColorIfNotSupported = ({ k, color = true, ...x }) => ({
  ...x,
  color: supportsColor.stdout && color,
  k: supportsColor.stdout && k
})

// const type = x => typeof x
const remapInputs = ({ to, _: input, ...opts }) => {
  const src = input && input.length > 0 ? input[0] : null
  if (!src) {
    opts.stdin = true
  }
  return [to, opts, src]
}
const convertify = ([to, opts, input]) =>
  (to === "json" ? yaml2json : json2yaml)(opts, input).fork(
    die([to, opts, input]),
    console.log
  )

const cli = pipe(
  remapInputs,
  convertify
)

export default pipe(
  args(CONFIG.yargsOpts),
  trace("parsed config"),
  shutOffColorIfNotSupported,
  inspect(prop("color"), "color off"),
  pipe(argv => {
    if (argv.help) {
      console.log(help(argv))
      return
    }
    return cli(argv)
  })
)(process.argv.slice(2))
