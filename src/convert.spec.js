/* global expect, test */
import { parseJSON, parseYAML } from "./convert"

test("parseJSON", done => {
  const input =
    '{"A": {"B": {"C": {"one": [0, 1, 2], "two": true, "three": false}}}}'
  return parseJSON(input).fork(done, actual => {
    expect(actual).toEqual({
      A: { B: { C: { one: [0, 1, 2], three: false, two: true } } }
    })
    done()
  })
})

test("parseYAML", () => {
  const input = `
what:
  the:
    hell:
      numbers:
      - 2
      - 4
      - 6
      truth: true
      falsehood: false
`
  const actual = parseYAML({}, input)
  const expected = {
    what: {
      the: { hell: { numbers: [2, 4, 6], truth: true, falsehood: false } }
    }
  }
  expect(actual).toEqual(expected)
})
test("parseYAML - safe", () => {
  const input = `
what:
  the:
    hell:
      numbers:
      - 2
      - 4
      - 6
      truth: true
      falsehood: false
`
  const actual = parseYAML({ safe: true }, input)
  const expected = {
    what: {
      the: { hell: { numbers: [2, 4, 6], truth: true, falsehood: false } }
    }
  }
  expect(actual).toEqual(expected)
})
