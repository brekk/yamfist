import { encase } from "fluture"
import { curry, flip } from "ramda"
import yaml from "js-yaml"

/**
 * read json from string
 * @protected
 * @method parseJSON
 * @param {string} raw - raw value to read
 * @return {Future<object>} future-wrapped value
 */
export const parseJSON = encase(JSON.parse)

/**
 * read yaml from string
 * @protected
 * @method parseYAML
 * @param {object} opts - options object, passes to yaml.safeLoad / yaml.load
 * @param {boolean} opts.safe - run in safe mode?
 * @param {string} x - raw data to read
 * @return {object} parsed yaml
 */
export const parseYAML = curry((opts, x) =>
  yaml[opts.safe ? `safeLoad` : `load`](x, opts)
)

/**
 * convert json to yaml
 * @protected
 * @method toYAML
 * @param {object} opts - an options object to pass to yaml.dump
 * @param {object} raw - raw data to read
 * @return {string} yaml-formatted object
 */
export const toYAML = flip(yaml.dump)
