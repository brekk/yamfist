#!/usr/bin/env node
'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var argify = _interopDefault(require('yargs-parser'));
var ramda = require('ramda');
var ramda__default = _interopDefault(ramda);
var os = _interopDefault(require('os'));
var cliui = _interopDefault(require('cliui'));
var kleur = _interopDefault(require('kleur'));
var tty = _interopDefault(require('tty'));
var util = _interopDefault(require('util'));
var fluture = require('fluture');
var fluture__default = _interopDefault(fluture);
var getStdin = _interopDefault(require('get-stdin'));
var path = _interopDefault(require('path'));
var fs = _interopDefault(require('fs'));
var yaml = _interopDefault(require('js-yaml'));

var hasFlag = function (flag, argv) {
	argv = argv || process.argv;
	var prefix = flag.startsWith('-') ? '' : (flag.length === 1 ? '-' : '--');
	var pos = argv.indexOf(prefix + flag);
	var terminatorPos = argv.indexOf('--');
	return pos !== -1 && (terminatorPos === -1 ? true : pos < terminatorPos);
};

var env = process.env;
var forceColor;
if (hasFlag('no-color') ||
	hasFlag('no-colors') ||
	hasFlag('color=false') ||
	hasFlag('color=never')) {
	forceColor = 0;
} else if (hasFlag('color') ||
	hasFlag('colors') ||
	hasFlag('color=true') ||
	hasFlag('color=always')) {
	forceColor = 1;
}
if ('FORCE_COLOR' in env) {
	if (env.FORCE_COLOR === true || env.FORCE_COLOR === 'true') {
		forceColor = 1;
	} else if (env.FORCE_COLOR === false || env.FORCE_COLOR === 'false') {
		forceColor = 0;
	} else {
		forceColor = env.FORCE_COLOR.length === 0 ? 1 : Math.min(parseInt(env.FORCE_COLOR, 10), 3);
	}
}
function translateLevel(level) {
	if (level === 0) {
		return false;
	}
	return {
		level: level,
		hasBasic: true,
		has256: level >= 2,
		has16m: level >= 3
	};
}
function supportsColor(stream) {
	if (forceColor === 0) {
		return 0;
	}
	if (hasFlag('color=16m') ||
		hasFlag('color=full') ||
		hasFlag('color=truecolor')) {
		return 3;
	}
	if (hasFlag('color=256')) {
		return 2;
	}
	if (stream && !stream.isTTY && forceColor === undefined) {
		return 0;
	}
	var min = forceColor || 0;
	if (env.TERM === 'dumb') {
		return min;
	}
	if (process.platform === 'win32') {
		var osRelease = os.release().split('.');
		if (
			Number(process.versions.node.split('.')[0]) >= 8 &&
			Number(osRelease[0]) >= 10 &&
			Number(osRelease[2]) >= 10586
		) {
			return Number(osRelease[2]) >= 14931 ? 3 : 2;
		}
		return 1;
	}
	if ('CI' in env) {
		if (['TRAVIS', 'CIRCLECI', 'APPVEYOR', 'GITLAB_CI'].some(function (sign) { return sign in env; }) || env.CI_NAME === 'codeship') {
			return 1;
		}
		return min;
	}
	if ('TEAMCITY_VERSION' in env) {
		return /^(9\.(0*[1-9]\d*)\.|\d{2,}\.)/.test(env.TEAMCITY_VERSION) ? 1 : 0;
	}
	if (env.COLORTERM === 'truecolor') {
		return 3;
	}
	if ('TERM_PROGRAM' in env) {
		var version = parseInt((env.TERM_PROGRAM_VERSION || '').split('.')[0], 10);
		switch (env.TERM_PROGRAM) {
			case 'iTerm.app':
				return version >= 3 ? 3 : 2;
			case 'Apple_Terminal':
				return 2;
		}
	}
	if (/-256(color)?$/i.test(env.TERM)) {
		return 2;
	}
	if (/^screen|^xterm|^vt100|^vt220|^rxvt|color|ansi|cygwin|linux/i.test(env.TERM)) {
		return 1;
	}
	if ('COLORTERM' in env) {
		return 1;
	}
	return min;
}
function getSupportLevel(stream) {
	var level = supportsColor(stream);
	return translateLevel(level);
}
var supportsColor_1 = {
	supportsColor: getSupportLevel,
	stdout: getSupportLevel(process.stdout),
	stderr: getSupportLevel(process.stderr)
};

function unwrapExports (x) {
	return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x['default'] : x;
}

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var ansiRegex = function (options) {
	options = Object.assign({
		onlyFirst: false
	}, options);
	var pattern = [
		'[\\u001B\\u009B][[\\]()#;?]*(?:(?:(?:[a-zA-Z\\d]*(?:;[-a-zA-Z\\d\\/#&.:=?%@~_]*)*)?\\u0007)',
		'(?:(?:\\d{1,4}(?:;\\d{0,4})*)?[\\dA-PR-TZcf-ntqry=><~]))'
	].join('|');
	return new RegExp(pattern, options.onlyFirst ? undefined : 'g');
};

var stripAnsi = function (string) { return typeof string === 'string' ? string.replace(ansiRegex(), '') : string; };
var stripAnsi_1 = stripAnsi;
var default_1 = stripAnsi;
stripAnsi_1.default = default_1;

var help = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, '__esModule', { value: true });
function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }
var cliui$1 = _interopDefault(cliui);
var color = _interopDefault(kleur);
var stripAnsi = _interopDefault(stripAnsi_1);
function _slicedToArray(arr, i) {
  return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest();
}
function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
}
function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; }
    return arr2;
  }
}
function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) { return arr; }
}
function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") { return Array.from(iter); }
}
function _iterableToArrayLimit(arr, i) {
  var _arr = [];
  var _n = true;
  var _d = false;
  var _e = undefined;
  try {
    for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);
      if (i && _arr.length === i) { break; }
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) { _i["return"](); }
    } finally {
      if (_d) { throw _e; }
    }
  }
  return _arr;
}
function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}
function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance");
}
var _map = [color.red, color.yellow, color.bold, color.underline].map(function (z) {
  return function (withColor) {
    return function () {
      return (withColor ? z : ramda__default.identity).apply(void 0, arguments);
    };
  };
}),
    _map2 = _slicedToArray(_map, 4),
    red = _map2[0],
    yellow = _map2[1],
    bold = _map2[2],
    underline = _map2[3];
var matchesTypeFromConfig = ramda__default.curry(function (config, type, x) {
  return ramda__default.pipe(ramda__default.propOr([], type), ramda__default.find(ramda__default.equals(x)))(config);
});
var flag = function flag(z) {
  return (stripAnsi(z).length === 1 ? "-" : "--") + z;
};
var flagify = ramda__default.curry(function (useColors, flags) {
  return ramda__default.pipe(ramda__default.map(ramda__default.pipe(yellow(useColors), flag)), ramda__default.join(", "))(flags);
});
var wrapChars = ramda__default.curry(function (a, b) {
  return a[0] + b + a[1];
});
var TYPES = ["string", "boolean", "array", "number"];
var getRawDescriptionsOrThrow = ramda__default.curry(function (w, x) {
  return ramda__default.pipe(ramda__default.pathOr("TBD", ["raw", "descriptions", x.name]), function (d) {
    if (d === "TBD") {
      throw new Error("".concat(x.name, " needs a description!"));
    }
    return d;
  })(w);
});
var getDefaults = ramda__default.curry(function (w, x) {
  return ramda__default.pathOr(" ", ["raw", "yargsOpts", "default", x.name], w);
});
var getType = ramda__default.curry(function (w, x) {
  var matcher = matchesTypeFromConfig(w.raw.yargsOpts);
  var findFlag = function findFlag(t) {
    return ramda__default.find(matcher(t), x.flags) ? t : false;
  };
  var type = TYPES.map(findFlag).find(ramda__default.identity);
  return type;
});
var convertFlag = ramda__default.curry(function (w, x) {
  var type = getType(w, x);
  var description = getRawDescriptionsOrThrow(w, x);
  var def = getDefaults(w, x);
  return Object.assign({}, x, {
    type: type,
    description: description,
    "default": def
  });
});
var getFlagInformation = function getFlagInformation(conf) {
  return ramda__default.pipe(ramda__default.path(["yargsOpts", "alias"]), ramda__default.toPairs, ramda__default.map(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 2),
        k = _ref2[0],
        v = _ref2[1];
    return {
      flags: [k].concat(_toConsumableArray(v)),
      name: k
    };
  }), ramda__default.reduce(function (_ref3, y) {
    var raw = _ref3.raw,
        data = _ref3.data;
    return {
      raw: raw,
      data: data.concat(y)
    };
  }, {
    raw: conf,
    data: []
  }), function (w) {
    return ramda__default.map(convertFlag(w), w.data);
  })(conf);
};
var getDefaultDiv = function getDefaultDiv(def) {
  return def !== " " ? {
    text: "(default: ".concat(def, ")"),
    align: "right",
    padding: [0, 2, 0, 0]
  } : def;
};
var helpWithOptions = ramda__default.curry(function (conf, argv) {
  var useColors = argv.color;
  var ui = cliui$1();
  var flags = getFlagInformation(conf);
  ui.div("\n".concat(underline(useColors)("Usage:"), " ").concat(bold(useColors)(conf.name), " <flags> [input]\n"));
  ui.div("".concat(underline(useColors)("Flags:"), "\n"));
  flags.forEach(function (_ref4) {
    var def = _ref4["default"],
        tags = _ref4.flags,
        description = _ref4.description,
        type = _ref4.type;
    return ui.div({
      text: flagify(useColors, tags),
      padding: [0, 0, 1, 1],
      align: "left"
    }, {
      text: ramda__default.pipe(red(useColors), wrapChars("[]"))(type),
      width: 15,
      padding: [0, 1, 0, 1],
      align: "center"
    }, {
      text: description,
      width: 36
    }, getDefaultDiv(def));
  });
  return ui.toString();
});
exports.convertFlag = convertFlag;
exports.flag = flag;
exports.flagify = flagify;
exports.getDefaults = getDefaults;
exports.getFlagInformation = getFlagInformation;
exports.getRawDescriptionsOrThrow = getRawDescriptionsOrThrow;
exports.helpWithOptions = helpWithOptions;
exports.matchesTypeFromConfig = matchesTypeFromConfig;
exports.wrapChars = wrapChars;
});
unwrapExports(help);
var help_1 = help.convertFlag;
var help_2 = help.flag;
var help_3 = help.flagify;
var help_4 = help.getDefaults;
var help_5 = help.getFlagInformation;
var help_6 = help.getRawDescriptionsOrThrow;
var help_7 = help.helpWithOptions;
var help_8 = help.matchesTypeFromConfig;
var help_9 = help.wrapChars;

var s = 1000;
var m = s * 60;
var h = m * 60;
var d = h * 24;
var w = d * 7;
var y = d * 365.25;
var ms = function(val, options) {
  options = options || {};
  var type = typeof val;
  if (type === 'string' && val.length > 0) {
    return parse(val);
  } else if (type === 'number' && isNaN(val) === false) {
    return options.long ? fmtLong(val) : fmtShort(val);
  }
  throw new Error(
    'val is not a non-empty string or a valid number. val=' +
      JSON.stringify(val)
  );
};
function parse(str) {
  str = String(str);
  if (str.length > 100) {
    return;
  }
  var match = /^((?:\d+)?\-?\d?\.?\d+) *(milliseconds?|msecs?|ms|seconds?|secs?|s|minutes?|mins?|m|hours?|hrs?|h|days?|d|weeks?|w|years?|yrs?|y)?$/i.exec(
    str
  );
  if (!match) {
    return;
  }
  var n = parseFloat(match[1]);
  var type = (match[2] || 'ms').toLowerCase();
  switch (type) {
    case 'years':
    case 'year':
    case 'yrs':
    case 'yr':
    case 'y':
      return n * y;
    case 'weeks':
    case 'week':
    case 'w':
      return n * w;
    case 'days':
    case 'day':
    case 'd':
      return n * d;
    case 'hours':
    case 'hour':
    case 'hrs':
    case 'hr':
    case 'h':
      return n * h;
    case 'minutes':
    case 'minute':
    case 'mins':
    case 'min':
    case 'm':
      return n * m;
    case 'seconds':
    case 'second':
    case 'secs':
    case 'sec':
    case 's':
      return n * s;
    case 'milliseconds':
    case 'millisecond':
    case 'msecs':
    case 'msec':
    case 'ms':
      return n;
    default:
      return undefined;
  }
}
function fmtShort(ms) {
  var msAbs = Math.abs(ms);
  if (msAbs >= d) {
    return Math.round(ms / d) + 'd';
  }
  if (msAbs >= h) {
    return Math.round(ms / h) + 'h';
  }
  if (msAbs >= m) {
    return Math.round(ms / m) + 'm';
  }
  if (msAbs >= s) {
    return Math.round(ms / s) + 's';
  }
  return ms + 'ms';
}
function fmtLong(ms) {
  var msAbs = Math.abs(ms);
  if (msAbs >= d) {
    return plural(ms, msAbs, d, 'day');
  }
  if (msAbs >= h) {
    return plural(ms, msAbs, h, 'hour');
  }
  if (msAbs >= m) {
    return plural(ms, msAbs, m, 'minute');
  }
  if (msAbs >= s) {
    return plural(ms, msAbs, s, 'second');
  }
  return ms + ' ms';
}
function plural(ms, msAbs, n, name) {
  var isPlural = msAbs >= n * 1.5;
  return Math.round(ms / n) + ' ' + name + (isPlural ? 's' : '');
}

function setup(env) {
	createDebug.debug = createDebug;
	createDebug.default = createDebug;
	createDebug.coerce = coerce;
	createDebug.disable = disable;
	createDebug.enable = enable;
	createDebug.enabled = enabled;
	createDebug.humanize = ms;
	Object.keys(env).forEach(function (key) {
		createDebug[key] = env[key];
	});
	createDebug.instances = [];
	createDebug.names = [];
	createDebug.skips = [];
	createDebug.formatters = {};
	function selectColor(namespace) {
		var hash = 0;
		for (var i = 0; i < namespace.length; i++) {
			hash = ((hash << 5) - hash) + namespace.charCodeAt(i);
			hash |= 0;
		}
		return createDebug.colors[Math.abs(hash) % createDebug.colors.length];
	}
	createDebug.selectColor = selectColor;
	function createDebug(namespace) {
		var prevTime;
		function debug() {
			var args = [], len = arguments.length;
			while ( len-- ) args[ len ] = arguments[ len ];
			if (!debug.enabled) {
				return;
			}
			var self = debug;
			var curr = Number(new Date());
			var ms = curr - (prevTime || curr);
			self.diff = ms;
			self.prev = prevTime;
			self.curr = curr;
			prevTime = curr;
			args[0] = createDebug.coerce(args[0]);
			if (typeof args[0] !== 'string') {
				args.unshift('%O');
			}
			var index = 0;
			args[0] = args[0].replace(/%([a-zA-Z%])/g, function (match, format) {
				if (match === '%%') {
					return match;
				}
				index++;
				var formatter = createDebug.formatters[format];
				if (typeof formatter === 'function') {
					var val = args[index];
					match = formatter.call(self, val);
					args.splice(index, 1);
					index--;
				}
				return match;
			});
			createDebug.formatArgs.call(self, args);
			var logFn = self.log || createDebug.log;
			logFn.apply(self, args);
		}
		debug.namespace = namespace;
		debug.enabled = createDebug.enabled(namespace);
		debug.useColors = createDebug.useColors();
		debug.color = selectColor(namespace);
		debug.destroy = destroy;
		debug.extend = extend;
		if (typeof createDebug.init === 'function') {
			createDebug.init(debug);
		}
		createDebug.instances.push(debug);
		return debug;
	}
	function destroy() {
		var index = createDebug.instances.indexOf(this);
		if (index !== -1) {
			createDebug.instances.splice(index, 1);
			return true;
		}
		return false;
	}
	function extend(namespace, delimiter) {
		var newDebug = createDebug(this.namespace + (typeof delimiter === 'undefined' ? ':' : delimiter) + namespace);
		newDebug.log = this.log;
		return newDebug;
	}
	function enable(namespaces) {
		createDebug.save(namespaces);
		createDebug.names = [];
		createDebug.skips = [];
		var i;
		var split = (typeof namespaces === 'string' ? namespaces : '').split(/[\s,]+/);
		var len = split.length;
		for (i = 0; i < len; i++) {
			if (!split[i]) {
				continue;
			}
			namespaces = split[i].replace(/\*/g, '.*?');
			if (namespaces[0] === '-') {
				createDebug.skips.push(new RegExp('^' + namespaces.substr(1) + '$'));
			} else {
				createDebug.names.push(new RegExp('^' + namespaces + '$'));
			}
		}
		for (i = 0; i < createDebug.instances.length; i++) {
			var instance = createDebug.instances[i];
			instance.enabled = createDebug.enabled(instance.namespace);
		}
	}
	function disable() {
		var namespaces = createDebug.names.map(toNamespace).concat( createDebug.skips.map(toNamespace).map(function (namespace) { return '-' + namespace; })
		).join(',');
		createDebug.enable('');
		return namespaces;
	}
	function enabled(name) {
		if (name[name.length - 1] === '*') {
			return true;
		}
		var i;
		var len;
		for (i = 0, len = createDebug.skips.length; i < len; i++) {
			if (createDebug.skips[i].test(name)) {
				return false;
			}
		}
		for (i = 0, len = createDebug.names.length; i < len; i++) {
			if (createDebug.names[i].test(name)) {
				return true;
			}
		}
		return false;
	}
	function toNamespace(regexp) {
		return regexp.toString()
			.substring(2, regexp.toString().length - 2)
			.replace(/\.\*\?$/, '*');
	}
	function coerce(val) {
		if (val instanceof Error) {
			return val.stack || val.message;
		}
		return val;
	}
	createDebug.enable(createDebug.load());
	return createDebug;
}
var common = setup;

var browser = createCommonjsModule(function (module, exports) {
exports.log = log;
exports.formatArgs = formatArgs;
exports.save = save;
exports.load = load;
exports.useColors = useColors;
exports.storage = localstorage();
exports.colors = [
	'#0000CC',
	'#0000FF',
	'#0033CC',
	'#0033FF',
	'#0066CC',
	'#0066FF',
	'#0099CC',
	'#0099FF',
	'#00CC00',
	'#00CC33',
	'#00CC66',
	'#00CC99',
	'#00CCCC',
	'#00CCFF',
	'#3300CC',
	'#3300FF',
	'#3333CC',
	'#3333FF',
	'#3366CC',
	'#3366FF',
	'#3399CC',
	'#3399FF',
	'#33CC00',
	'#33CC33',
	'#33CC66',
	'#33CC99',
	'#33CCCC',
	'#33CCFF',
	'#6600CC',
	'#6600FF',
	'#6633CC',
	'#6633FF',
	'#66CC00',
	'#66CC33',
	'#9900CC',
	'#9900FF',
	'#9933CC',
	'#9933FF',
	'#99CC00',
	'#99CC33',
	'#CC0000',
	'#CC0033',
	'#CC0066',
	'#CC0099',
	'#CC00CC',
	'#CC00FF',
	'#CC3300',
	'#CC3333',
	'#CC3366',
	'#CC3399',
	'#CC33CC',
	'#CC33FF',
	'#CC6600',
	'#CC6633',
	'#CC9900',
	'#CC9933',
	'#CCCC00',
	'#CCCC33',
	'#FF0000',
	'#FF0033',
	'#FF0066',
	'#FF0099',
	'#FF00CC',
	'#FF00FF',
	'#FF3300',
	'#FF3333',
	'#FF3366',
	'#FF3399',
	'#FF33CC',
	'#FF33FF',
	'#FF6600',
	'#FF6633',
	'#FF9900',
	'#FF9933',
	'#FFCC00',
	'#FFCC33'
];
function useColors() {
	if (typeof window !== 'undefined' && window.process && (window.process.type === 'renderer' || window.process.__nwjs)) {
		return true;
	}
	if (typeof navigator !== 'undefined' && navigator.userAgent && navigator.userAgent.toLowerCase().match(/(edge|trident)\/(\d+)/)) {
		return false;
	}
	return (typeof document !== 'undefined' && document.documentElement && document.documentElement.style && document.documentElement.style.WebkitAppearance) ||
		(typeof window !== 'undefined' && window.console && (window.console.firebug || (window.console.exception && window.console.table))) ||
		(typeof navigator !== 'undefined' && navigator.userAgent && navigator.userAgent.toLowerCase().match(/firefox\/(\d+)/) && parseInt(RegExp.$1, 10) >= 31) ||
		(typeof navigator !== 'undefined' && navigator.userAgent && navigator.userAgent.toLowerCase().match(/applewebkit\/(\d+)/));
}
function formatArgs(args) {
	args[0] = (this.useColors ? '%c' : '') +
		this.namespace +
		(this.useColors ? ' %c' : ' ') +
		args[0] +
		(this.useColors ? '%c ' : ' ') +
		'+' + module.exports.humanize(this.diff);
	if (!this.useColors) {
		return;
	}
	var c = 'color: ' + this.color;
	args.splice(1, 0, c, 'color: inherit');
	var index = 0;
	var lastC = 0;
	args[0].replace(/%[a-zA-Z%]/g, function (match) {
		if (match === '%%') {
			return;
		}
		index++;
		if (match === '%c') {
			lastC = index;
		}
	});
	args.splice(lastC, 0, c);
}
function log() {
	var args = [], len = arguments.length;
	while ( len-- ) args[ len ] = arguments[ len ];
	return typeof console === 'object' &&
		console.log &&
		console.log.apply(console, args);
}
function save(namespaces) {
	try {
		if (namespaces) {
			exports.storage.setItem('debug', namespaces);
		} else {
			exports.storage.removeItem('debug');
		}
	} catch (error) {
	}
}
function load() {
	var r;
	try {
		r = exports.storage.getItem('debug');
	} catch (error) {
	}
	if (!r && typeof process !== 'undefined' && 'env' in process) {
		r = process.env.DEBUG;
	}
	return r;
}
function localstorage() {
	try {
		return localStorage;
	} catch (error) {
	}
}
module.exports = common(exports);
var ref = module.exports;
var formatters = ref.formatters;
formatters.j = function (v) {
	try {
		return JSON.stringify(v);
	} catch (error) {
		return '[UnexpectedJSONParseError]: ' + error.message;
	}
};
});
var browser_1 = browser.log;
var browser_2 = browser.formatArgs;
var browser_3 = browser.save;
var browser_4 = browser.load;
var browser_5 = browser.useColors;
var browser_6 = browser.storage;
var browser_7 = browser.colors;

var node = createCommonjsModule(function (module, exports) {
exports.init = init;
exports.log = log;
exports.formatArgs = formatArgs;
exports.save = save;
exports.load = load;
exports.useColors = useColors;
exports.colors = [6, 2, 3, 4, 5, 1];
try {
	var supportsColor = supportsColor_1;
	if (supportsColor && (supportsColor.stderr || supportsColor).level >= 2) {
		exports.colors = [
			20,
			21,
			26,
			27,
			32,
			33,
			38,
			39,
			40,
			41,
			42,
			43,
			44,
			45,
			56,
			57,
			62,
			63,
			68,
			69,
			74,
			75,
			76,
			77,
			78,
			79,
			80,
			81,
			92,
			93,
			98,
			99,
			112,
			113,
			128,
			129,
			134,
			135,
			148,
			149,
			160,
			161,
			162,
			163,
			164,
			165,
			166,
			167,
			168,
			169,
			170,
			171,
			172,
			173,
			178,
			179,
			184,
			185,
			196,
			197,
			198,
			199,
			200,
			201,
			202,
			203,
			204,
			205,
			206,
			207,
			208,
			209,
			214,
			215,
			220,
			221
		];
	}
} catch (error) {
}
exports.inspectOpts = Object.keys(process.env).filter(function (key) {
	return /^debug_/i.test(key);
}).reduce(function (obj, key) {
	var prop = key
		.substring(6)
		.toLowerCase()
		.replace(/_([a-z])/g, function (_, k) {
			return k.toUpperCase();
		});
	var val = process.env[key];
	if (/^(yes|on|true|enabled)$/i.test(val)) {
		val = true;
	} else if (/^(no|off|false|disabled)$/i.test(val)) {
		val = false;
	} else if (val === 'null') {
		val = null;
	} else {
		val = Number(val);
	}
	obj[prop] = val;
	return obj;
}, {});
function useColors() {
	return 'colors' in exports.inspectOpts ?
		Boolean(exports.inspectOpts.colors) :
		tty.isatty(process.stderr.fd);
}
function formatArgs(args) {
	var ref = this;
	var name = ref.namespace;
	var useColors = ref.useColors;
	if (useColors) {
		var c = this.color;
		var colorCode = '\u001B[3' + (c < 8 ? c : '8;5;' + c);
		var prefix = "  " + colorCode + ";1m" + name + " \u001b[0m";
		args[0] = prefix + args[0].split('\n').join('\n' + prefix);
		args.push(colorCode + 'm+' + module.exports.humanize(this.diff) + '\u001B[0m');
	} else {
		args[0] = getDate() + name + ' ' + args[0];
	}
}
function getDate() {
	if (exports.inspectOpts.hideDate) {
		return '';
	}
	return new Date().toISOString() + ' ';
}
function log() {
	var args = [], len = arguments.length;
	while ( len-- ) args[ len ] = arguments[ len ];
	return process.stderr.write(util.format.apply(util, args) + '\n');
}
function save(namespaces) {
	if (namespaces) {
		process.env.DEBUG = namespaces;
	} else {
		delete process.env.DEBUG;
	}
}
function load() {
	return process.env.DEBUG;
}
function init(debug) {
	debug.inspectOpts = {};
	var keys = Object.keys(exports.inspectOpts);
	for (var i = 0; i < keys.length; i++) {
		debug.inspectOpts[keys[i]] = exports.inspectOpts[keys[i]];
	}
}
module.exports = common(exports);
var ref = module.exports;
var formatters = ref.formatters;
formatters.o = function (v) {
	this.inspectOpts.colors = this.useColors;
	return util.inspect(v, this.inspectOpts)
		.replace(/\s*\n\s*/g, ' ');
};
formatters.O = function (v) {
	this.inspectOpts.colors = this.useColors;
	return util.inspect(v, this.inspectOpts);
};
});
var node_1 = node.init;
var node_2 = node.log;
var node_3 = node.formatArgs;
var node_4 = node.save;
var node_5 = node.load;
var node_6 = node.useColors;
var node_7 = node.colors;
var node_8 = node.inspectOpts;

var src = createCommonjsModule(function (module) {
if (typeof process === 'undefined' || process.type === 'renderer' || process.browser === true || process.__nwjs) {
	module.exports = browser;
} else {
	module.exports = node;
}
});

var log = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, '__esModule', { value: true });
function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }
var debug = _interopDefault(src);
var PLACEHOLDER = "🍛";
var $ = PLACEHOLDER;
var bindInternal3 = function bindInternal3(func, thisContext) {
  return function (a, b, c) {
    return func.call(thisContext, a, b, c);
  };
};
var some$1 = function fastSome(subject, fn, thisContext) {
  var length = subject.length,
      iterator = thisContext !== undefined ? bindInternal3(fn, thisContext) : fn,
      i;
  for (i = 0; i < length; i++) {
    if (iterator(subject[i], i, subject)) {
      return true;
    }
  }
  return false;
};
var curry = function curry(fn) {
  var test = function test(x) {
    return x === PLACEHOLDER;
  };
  return function curried() {
    var arguments$1 = arguments;
    var argLength = arguments.length;
    var args = new Array(argLength);
    for (var i = 0; i < argLength; ++i) {
      args[i] = arguments$1[i];
    }
    var countNonPlaceholders = function countNonPlaceholders(toCount) {
      var count = toCount.length;
      while (!test(toCount[count])) {
        count--;
      }
      return count;
    };
    var length = some$1(args, test) ? countNonPlaceholders(args) : args.length;
    function saucy() {
      var arguments$1 = arguments;
      var arg2Length = arguments.length;
      var args2 = new Array(arg2Length);
      for (var j = 0; j < arg2Length; ++j) {
        args2[j] = arguments$1[j];
      }
      return curried.apply(this, args.map(function (y) {
        return test(y) && args2[0] ? args2.shift() : y;
      }).concat(args2));
    }
    return length >= fn.length ? fn.apply(this, args) : saucy;
  };
};
var innerpipe = function innerpipe(args) {
  return function (x) {
    var first = args[0];
    var rest = args.slice(1);
    var current = first(x);
    for (var a = 0; a < rest.length; a++) {
      current = rest[a](current);
    }
    return current;
  };
};
function pipe() {
  var arguments$1 = arguments;
  var argLength = arguments.length;
  var args = new Array(argLength);
  for (var i = 0; i < argLength; ++i) {
    args[i] = arguments$1[i];
  }
  return innerpipe(args);
}
var prop = curry(function (property, o) {
  return o && property && o[property];
});
var _keys = Object.keys;
var keys = _keys;
var propLength = prop("length");
var objectLength = pipe(keys, propLength);
var delegatee = curry(function (method, arg, x) {
  return x[method](arg);
});
var filter = delegatee("filter");
function curryObjectN(arity, fn) {
  return function λcurryObjectN(args) {
    var joined = function joined(z) {
      return λcurryObjectN(Object.assign({}, args, z));
    };
    return args && Object.keys(args).length >= arity ? fn(args) : joined;
  };
}
var callWithScopeWhen = curry(function (effect, when, what, value) {
  var scope = what(value);
  if (when(scope)) { effect(scope); }
  return value;
});
var callBinaryWithScopeWhen = curry(function (effect, when, what, tag, value) {
  var scope = what(value);
  if (when(tag, scope)) { effect(tag, scope); }
  return value;
});
var always = function always() {
  return true;
};
var I = function I(x) {
  return x;
};
var callWhen = callWithScopeWhen($, $, I);
var call = callWithScopeWhen($, always, I);
var callWithScope = callWithScopeWhen($, always);
var callBinaryWhen = callBinaryWithScopeWhen($, $, I);
var callBinaryWithScope = callBinaryWithScopeWhen($, always);
var callBinary = callBinaryWithScopeWhen($, always, I);
var traceWithScopeWhen = callBinaryWithScopeWhen(console.log);
var traceWithScope = traceWithScopeWhen(always);
var inspect = traceWithScope;
var trace = inspect(I);
var traceWhen = callBinaryWithScopeWhen(console.log, $, I);
var segment = curryObjectN(3, function (_ref) {
  var _ref$what = _ref.what,
      what = _ref$what === void 0 ? I : _ref$what,
      _ref$when = _ref.when,
      when = _ref$when === void 0 ? always : _ref$when,
      tag = _ref.tag,
      value = _ref.value,
      effect = _ref.effect;
  if (when(tag, what(value))) {
    effect(tag, what(value));
  }
  return value;
});
var segmentTrace = segment({
  effect: console.log
});
var make = function make(maker) {
  return function (name) {
    var tagger = debug(name);
    return maker(tagger);
  };
};
var makeTracer = make(callBinary);
var makeInspector = ramda__default.pipe(function (k) {
  return "inspector:".concat(k);
}, make(callBinaryWithScope));
exports.makeInspector = makeInspector;
exports.makeTracer = makeTracer;
});
unwrapExports(log);
var log_1 = log.makeInspector;
var log_2 = log.makeTracer;

var cli = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, '__esModule', { value: true });
function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }
var F__default = _interopDefault(fluture__default);
var getStdin$1 = _interopDefault(getStdin);
var path$1 = _interopDefault(path);
var fs$1 = _interopDefault(fs);
var relativePathWithCWD = ramda__default.curryN(2, path$1.relative);
var readRaw = ramda__default.curryN(3, fs$1.readFile);
var readUTF8 = readRaw(ramda__default.__, "utf8");
var readFile = ramda__default.pipe(readUTF8, fluture__default.node);
var writeRaw = ramda__default.curryN(4, fs$1.writeFile);
var writeUTF8 = writeRaw(ramda__default.__, ramda__default.__, "utf8");
var writeFile = ramda__default.curry(function (to, data) {
  return ramda__default.pipe(writeUTF8(to), fluture__default.node)(data);
});
var readRelative = ramda__default.pipe(relativePathWithCWD(process.cwd()), readFile);
var readStdin = F__default.encaseP(getStdin$1);
var readWithOpts = ramda__default.curry(function (opts, source) {
  return (
    opts.stdin ?
    readStdin()
    : F__default.of(source)
  );
});
var ensureBinary = function ensureBinary(fn) {
  if (process.env.NODE_ENV !== "production") {
    if (typeof fn !== "function" || typeof fn("test") !== "function") {
      throw new TypeError("Expected to be given a curried binary function!");
    }
  }
  return fn;
};
var processAsync = ramda__default.curry(function (fn, opts, source) {
  return ramda__default.pipe(
  opts.file ? readRelative : readWithOpts(opts),
  ensureBinary(fn)(opts),
  opts.output ? ramda__default.chain(writeFile(opts.output)) : ramda__default.identity)(source);
});
exports.processAsync = processAsync;
exports.readRelative = readRelative;
exports.readStdin = readStdin;
exports.readWithOpts = readWithOpts;
});
unwrapExports(cli);
var cli_1 = cli.processAsync;
var cli_2 = cli.readRelative;
var cli_3 = cli.readStdin;
var cli_4 = cli.readWithOpts;

var parseJSON = fluture.encase(JSON.parse);
var parseYAML = ramda.curry(function (opts, x) { return yaml[opts.safe ? "safeLoad" : "load"](x, opts); }
);
var toYAML = ramda.flip(yaml.dump);

var json2yaml = cli_1(
  ramda.curry(function (opts, source) { return ramda.pipe(
      ramda.chain(parseJSON),
      ramda.map(toYAML(opts))
    )(source); }
  )
);
var yaml2json = cli_1(
  ramda.curry(function (opts, source) { return ramda.map(parseYAML(opts))(source); })
);

var yargsOpts = {
  alias: {
    file: ["f"],
    help: ["h"],
    output: ["o"],
    to: ["t"],
    color: ["k"],
    verbose: ["v"]
  },
  default: {
    color: true
  },
  count: ["v"],
  number: "v".split(""),
  string: "ot".split(""),
  boolean: "fhk".split("")
};
var CONFIG = {
  name: "yamfist",
  yargsOpts: yargsOpts,
  descriptions: {
    file: "read from the file system",
    output: "write to the file system",
    help: "get help",
    to: "set output format",
    color: "use --no-color to turn colors off",
    verbose: "set verbosity 1 - 3"
  }
};

function objectWithoutProperties (obj, exclude) { var target = {}; for (var k in obj) if (Object.prototype.hasOwnProperty.call(obj, k) && exclude.indexOf(k) === -1) target[k] = obj[k]; return target; }
var THIS = "yamfist:core";
var trace = log_2(THIS);
var inspect = log_1(THIS);
var args = ramda.curry(function (opts, x) { return argify(x, opts); });
var die = function (data) { return function (e) {
  console.warn(data);
  console.warn(e);
  process.exit(1);
}; };
var help$1 = help_7(CONFIG);
var shutOffColorIfNotSupported = function (ref) {
  var k = ref.k;
  var color = ref.color; if ( color === void 0 ) color = true;
  var rest = objectWithoutProperties( ref, ["k", "color"] );
  var x = rest;
  return (Object.assign({}, x,
  {color: supportsColor_1.stdout && color,
  k: supportsColor_1.stdout && k}));
};
var remapInputs = function (ref) {
  var to = ref.to;
  var input = ref._;
  var rest = objectWithoutProperties( ref, ["to", "_"] );
  var opts = rest;
  var src = input && input.length > 0 ? input[0] : null;
  if (!src) {
    opts.stdin = true;
  }
  return [to, opts, src]
};
var convertify = function (ref) {
    var to = ref[0];
    var opts = ref[1];
    var input = ref[2];
    return (to === "json" ? yaml2json : json2yaml)(opts, input).fork(
    die([to, opts, input]),
    console.log
  );
};
var cli$1 = ramda.pipe(
  remapInputs,
  convertify
);
var index = ramda.pipe(
  args(CONFIG.yargsOpts),
  trace("parsed config"),
  shutOffColorIfNotSupported,
  inspect(ramda.prop("color"), "color off"),
  ramda.pipe(function (argv) {
    if (argv.help) {
      console.log(help$1(argv));
      return
    }
    return cli$1(argv)
  })
)(process.argv.slice(2));

module.exports = index;
